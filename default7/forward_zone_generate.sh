#!/bin/sh
# First arg is domain you wish to generate a [forward] zone for
[ $# -lt 1 ] && printf "No domain supplied ... exiting\n" && exit 100
domain=$1

cat <<EOF > /tmp/${domain}.db
\$ORIGIN .
\$TTL 21600	; 6 hours (for testing we might instead set TTL 1)
${domain}               IN SOA  ns1.${domain}. hostmaster.${domain}. (
				2019010499	; serial
				60		; refresh - 1 minute
				15		; retry - 15 seconds)
				5M		; expire - 5 minutes (30 minutes would be 1800 instead)
				10		; minimum - 10 seconds
				)
                        NS      ns1.${domain}.
;;	 14400		IN A		138.253.13.50
                        MX      10 mail.${domain}.
\$ORIGIN ${domain}.
ns1			A	127.0.0.1
;;www	7200		IN A	138.253.13.50
EOF
