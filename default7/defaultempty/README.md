# missing by design - rndc.conf plus

Files in 'defaultempty' are either deliberately absent by default
or else deliberately empty by default


# rndc.conf - generally and specialised

Typical rndc setup has absent rndc.conf once installation complete

Even when rndc is initialised by you, it is still your choice on whether
to populate rndc.conf, or,
just accept the default configuration setup which sees absent rndc.conf

absent low effort setup: rndc-confgen -a -r /dev/urandom

Note: Use the next form only when you are skilled enough to complete that kind of config
specialised post configuration required: rndc-confgen -r /dev/urandom > /etc/rndc.conf




